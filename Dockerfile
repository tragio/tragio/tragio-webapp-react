FROM node:14.11.0-alpine3.11 as dev

# provide build args of UID and USER. Matching the host's UID during dev allows
# one to yield files to host fs w/ correct perms from the container
ARG WORKDIR=/app
ARG UID=1000

# build args are only at build-time...need to set inside container
ENV WORKDIR=${WORKDIR}
ENV UID=${UID}

WORKDIR $WORKDIR

# setup OS environment
RUN chown -R $UID:node /app \
    && echo "$WORKDIR CREATED WITH $UID:node AS OWNER"

FROM dev as build

COPY . /app

RUN yarn install \
    && yarn cache clean \
    && yarn run build

FROM nginx:1.19.2-alpine as deploy

# new in 1.19 var subst is built into conf templates...
RUN mkdir /etc/nginx/templates \
    && cp /etc/nginx/conf.d/default.conf /etc/nginx/templates/default.conf.template \
    && sed -i 's/80;/$NGINX_PORT;/g' /etc/nginx/templates/default.conf.template

COPY --from=build --chown=root:root /app/build /usr/share/nginx/html
