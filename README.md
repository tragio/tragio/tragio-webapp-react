# trag.io Web Application

## Docker Precepts

A common pattern used across services in this ecosystem is the multi-stage
Dockerfile.  Conceptually, four targets exist, although some may be flattened
together at times.  The first, `base`, is the layer which will _typically_ be
inherited by both `dev` and `deploy`.  This allows common scaffolding such as
in-container user setup, working directory, and common base requirements to be
installed one time and used in both contexts.  This is more critical when one
is not already extending a Docker image that has been groomed to one's liking.
From there, the `dev` layer is created, which extends the docker image with
any additional requirements required by the development or build environments.
Diagram and documentation production, test requirements, etc. should all be in
this layer, but not the code itself.  The concept is that the developer will
bind mount their current state of code into the container at development time
to ensure no incidental stale files are inside the container.  Next, the
`build` stage is intended ot be used by continuous integration (CI) builders.
It will copy the code in and build the production artifacts.  There is an
assumption by this design that the CI system has already done an out-of-band
build, test, and quality inspection of the code (using the `dev` layer and a
clone of the appropriate git branch/tag) before the "final" build happens.
Thus, this `build` layer finally copies the source code in and builds the
release artifacts. Finally, the `deploy` layer fulfills its duty as providing
an unpolluted, deployment-ready container with nothing but the basic
dependencies required to work and a clean copy of the artifacts from the
`build` layer.

## Docker Compose Precepts

Given a multi-stage Dockerfile that complies with the above pattern, two
committed docker-compose files ought to be produced.  The first, using the
reserved name `docker-compose.yml`, should be exactly the parameters to run
the production stack.  It should be "12-factored" and have sane defaults
insofar as possible.  The second, `docker-compose.dev.yml`, should extend
the `docker-compose.yml` services, as practical, to provide a suitable dev
environment.  This will often mean bind mounting a/some volume(s), and
introducing additional environment variables and build arguments.  This
name was chosen to a) make it easy to write helper scripts that scaffold
this file in for you, and/or b) allow one to copy the dev file into a
git-ignored `docker-compose.override.yml`, which is a reserved filename
for development overrides of docker compose files.

## Development Environment

### How the Docker environment was created

1.  The `Dockerfile` and `docker-compose.*.yml` files have been groomed as
stated above.

1.  At this time, only the `dev` target will work by design.  This layer is
to be used for development, to include initial creation of the project source
and yield of the development environment on the host.

### How the React project was created

1.  Using the `dev` environment a react project is created that mirrors the
name of the project directory and then the tree is merged:

    ```
    cp docker-compose.dev.yml docker-compose.override.yml
    docker-compose run --rm webapp npx create-react-app tragio-webapp
    # manually merge .gitignore and README.md
    mv tragio-webapp/* .
    rm -fr tragio-webapp
    ```

### Toggling between development and deployment

Now that the web application is available in the repository, one can safely
leverage the latter parts of the Dockerfile.  This can be accomplished by
renaming/removing the override file, or using a `-f docker-compose.yml` when
running `docker-compose` commands.  Remember to stop any running stacks
before switching context.

The following docs are merged from create-react-app.  You can run any of
them in the `dev` context by replacing the `yarn` command with:

```shell script
# if the stack is not running
docker-compose run --rm webapp yarn
# if the stack is running
docker-compose exec webapp yarn
``` 

### When cloning

When you first clone the repo, you will not have any dependencies.  Make sure
to run `docker-compose run --rm webapp yarn` prior to starting the app in `dev`
mode or building/starting the app in `deploy` mode.

---

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `yarn build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
